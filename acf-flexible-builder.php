<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.l7-marketing.com/
 * @since             1.2.9
 * @package           Acf_Flexible_Builder
 *
 * @wordpress-plugin
 * Plugin Name:       ACF Flexible Builder
 * Plugin URI:        https://www.l7-marketing.com/
 * Description:       ACF Flexible builder is a custom plugin that leverages the power of Advanced Customer Fields PRO to create easy to use building blocks to quickly build both an amazing custom site, and flexible responsible website leveraging state of the art UI/UX technologies. The plugin is licensed to L7 Marketing clients only.
 * Version:           1.2.9
 * Author:            L7
 * Author URI:        https://www.l7-marketing.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       acf-flexible-builder
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ACF_FLEXIBLE_BUILDER_VERSION', '1.2.9' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-acf-flexible-builder-activator.php
 */
function activate_acf_flexible_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acf-flexible-builder-activator.php';
	Acf_Flexible_Builder_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-acf-flexible-builder-deactivator.php
 */
function deactivate_acf_flexible_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acf-flexible-builder-deactivator.php';
	Acf_Flexible_Builder_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_acf_flexible_builder' );
register_deactivation_hook( __FILE__, 'deactivate_acf_flexible_builder' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-acf-flexible-builder.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.2.9
 */
function run_acf_flexible_builder() {

	$plugin = new Acf_Flexible_Builder();
	$plugin->run();

}
run_acf_flexible_builder();

/**
 * Update Plugin
 */
require_once plugin_dir_path( __FILE__ ) . 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/decenthq/acf-flexible-builder',
    __FILE__,
    'acf-flexible-builder'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
//$myUpdateChecker->setAuthentication(array(
//    'consumer_key' => '...',
//    'consumer_secret' => '...',
//));

//Optional: Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('stable-branch-name');
