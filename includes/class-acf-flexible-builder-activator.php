<?php

/**
 * Fired during plugin activation
 *
 * @link       decent.studio
 * @since      1.0.0
 *
 * @package    Acf_Flexible_Builder
 * @subpackage Acf_Flexible_Builder/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Acf_Flexible_Builder
 * @subpackage Acf_Flexible_Builder/includes
 * @author     Artem Anoshin <artem@dcent.ru>
 */
class Acf_Flexible_Builder_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
	}
		
}
