<?php

/**
 * Fired during plugin deactivation
 *
 * @link       decent.studio
 * @since      1.0.0
 *
 * @package    Acf_Flexible_Builder
 * @subpackage Acf_Flexible_Builder/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Acf_Flexible_Builder
 * @subpackage Acf_Flexible_Builder/includes
 * @author     Artem Anoshin <artem@dcent.ru>
 */
class Acf_Flexible_Builder_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
