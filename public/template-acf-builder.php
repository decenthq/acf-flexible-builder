<?php
/**
 * Template Name: ACF Flexible Page Builder
 * Template Post Type: page, portfolio
 */
get_header();
?>

<?php 
global $post;


$id = $post->ID;
$meta_values = get_post_meta( $id );

//echo '<pre>';
//print_r($meta_values);
//echo '</pre>';

?>

<!-- ACF Flexible Builder -->
<?php

if (have_rows('acf_flexible_builder', $id)) :

    #Container
    echo '<div class="acf-flexible-builder">';

    #Loop
    while (have_rows('acf_flexible_builder', $id)) : the_row();

        #hero
        if (get_row_layout() == 'hero') :

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/hero.php');


        elseif( get_row_layout() == 'text' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/text.php');

        elseif( get_row_layout() == 'fullwidth_cta' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/fullwidth-cta.php');

		elseif( get_row_layout() == 'fullwidth_image' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/fullwidth-image.php');

		elseif( get_row_layout() == 'grid' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/grid.php');

		elseif( get_row_layout() == 'form' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/form.php');

		elseif( get_row_layout() == 'notification' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/notification.php');

		elseif( get_row_layout() == 'logos' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/logos.php');

		elseif( get_row_layout() == 'featured_cta_with_images' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/featured-cta-with-images.php');

		elseif( get_row_layout() == 'featured_cta_s' ):

			include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/featured-cta-s.php');

        elseif( get_row_layout() == 'divider' ):

	        include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/divider.php');

        elseif( get_row_layout() == 'gallery' ):

	        include( plugin_dir_path( __FILE__ ) . 'template-parts/acf-flexible-builder-parts/gallery.php');

        endif;

    endwhile;

    #Container
    echo '</div>';

endif;

?>

<?php 
get_footer();