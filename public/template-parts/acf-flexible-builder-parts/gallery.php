<?php

$images = get_sub_field('gallery');

if( $images ): ?>
    <div class="builder-gallery-container">
	<?php foreach( $images as $image ): ?>
        <div class="builder-gallery-item wow fadeIn" data-wow-duration="2s">
            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
        </div>
	<?php endforeach; ?>
    </div>
<?php endif;