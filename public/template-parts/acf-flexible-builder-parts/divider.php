<?php

$desktopHeight = get_sub_field('desktop_height');
$mobileHeight = get_sub_field('mobile_height');
$bgColor = get_sub_field('bg_color');

?>

<div id="acf-flexible-builder-divider-<?php echo get_row_index(); ?>"></div>

<style>
    #acf-flexible-builder-divider-<?php echo get_row_index(); ?> {
        height: <?php echo $desktopHeight; ?>px;
        background-color: <?php echo $bgColor; ?>;
    }

    @media (max-width: 720px) {
        #acf-flexible-builder-divider-<?php echo get_row_index(); ?> {
            height: <?php echo $mobileHeight; ?>px;
        }
    }
</style>
